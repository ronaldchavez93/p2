# RESTful API convertUTC en Spring boot #

Este es un proyecto de código abierto para convertir la hora a formato UTC implementada con Spring boot Framework.

### Requerimientos ###

* Java JDK 11
* Maven
* Spring Tools Suite
* Git


### Test ###

* Host: https://convert-utc.herokuapp.com/api/formato-utc
* Metodo: post
* Body : {
		  zone: "+4",
		  time: "hh:mm:ss"
		}
