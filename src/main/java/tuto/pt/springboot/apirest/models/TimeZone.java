package tuto.pt.springboot.apirest.models;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class TimeZone {
	
	@NotEmpty
	@Pattern(regexp = "[0-9]{2}[:][0-9]{2}[:][0-9]{2}",
			message = "tiene el formato incorrecto (hh:mm:ss)")
	private String time;
	
	@NotEmpty
	private String zone;

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}
	
	

}
