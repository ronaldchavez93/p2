package tuto.pt.springboot.apirest.controllers;


import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tuto.pt.springboot.apirest.models.TimeZone;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class FormatoUtcRestController {
	

	@PostMapping("/formato-utc")
	public ResponseEntity<?> create(@Valid @RequestBody TimeZone timeZone, BindingResult result) {
		
		Map<String, Object> response = new HashMap<>();
		Map<String, Object> data = new HashMap<>();
		
		/* VALIDACIONES */
		if (result.hasErrors()) {
			List<String> errors = new ArrayList<>();

			for (FieldError err : result.getFieldErrors()) {
				errors.add("El campo '" + err.getField() + "' " + err.getDefaultMessage());
			}

			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		/* FIN */

		try {
			
			/* HORA EN FORMATO hh:mm:ss */
			String time = timeZone.getTime();
			/* ZORA HORARIA EJEMPLO (+4) */
			String zone = timeZone.getZone();
			
			/* SE CONVIERTE LA HORA EN ARRAY PARA PASARSELA AL METODO LOCALTIME.OF(hh, mm, ss) */
			String[] timeDate = time.split(":");
			
			ZonedDateTime zonedFechaExpiracion = ZonedDateTime
		               .now(ZoneId.of(zone)) /* ZONA HORARIA */
		               .with(LocalTime.of( 	Integer.parseInt(timeDate[0]), 
		            		   				Integer.parseInt(timeDate[1]), 
		            		   				Integer.parseInt(timeDate[2]) )) /* SE ASIGNA LA HORA QUE SE RECIBE (hh, mm, ss) */
		               .withZoneSameInstant(ZoneOffset.UTC); /* CONVERTIMOS A FORMATO UTC */
			
			/* FORMA DE HORA */
		    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
		    /* FIN */
		    
		    data.put("time", zonedFechaExpiracion.format(formatter));
		    data.put("timezone", "utc");
			response.put("response", data);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

		} catch (Exception e) {

			response.put("message", "Error al realizar la conversión a UTC");
			response.put("error", e.getMessage().concat(": ").concat(e.getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

}
